import pickle
import io


def main():

    freq_dict = {}
    with io.open("dfl.cd", 'r', encoding="utf8") as f:
        file = f.readlines()
    for line in file:
        splitted_line = line.split('\\')
        word = splitted_line[1].encode("utf-8")
        frequency = splitted_line[2].encode("utf-8")
        freq_dict[word] = frequency

    with open("Celex_freq.p", "wb") as file:
        pickle.dump(freq_dict, file)

if __name__ == '__main__':
    main()