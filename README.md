# Scriptie Hilde Jongeling

Hoe kom je tot mijn resultaat door middel van het gebruik van deze repository?

1. Download en installeer Treetagger.
2. Heb je het zoals ik in Windows gedownload, ga dan naar je Windows Command Prompt en typ in “tag-dutch try.txt>treetagger-output.txt” 
3. Run read_lemma.py met het bestand wat bij de vorige stap als output kwam.
4. Run frequency_Celex.py with the frequency file of the Celex corpus.
5. Run frequency_Wablieft.py with the frequency file of the Wablieft corpus.
6. Run Lexical_simplification.py.

