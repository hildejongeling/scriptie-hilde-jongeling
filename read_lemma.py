import pickle
import io

"Door TreeTagger worden de zinnen getagd en de lemma's in een bestand gestopt. Dit programma leest dat bestand en stopt de woorden en hun lemma's in een pickle bestand."

def main():

    lemma_dict = {}
    with io.open("test-output.txt", 'r', encoding="utf8") as f:
        textfile = f.readlines()
    for line in textfile:
        stripped_line = line.rstrip()
        tags = stripped_line.split('\t')
        key_word = tags[0].replace("'s", "")
        value_word = tags[2].replace("'s", "")
        lemma_dict[key_word.encode("utf-8")] = value_word.encode("utf-8")
    with open("Lemmas_text.p", "wb") as file:
        pickle.dump(lemma_dict, file)

if __name__ == '__main__':
    main()