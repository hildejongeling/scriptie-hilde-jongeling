import pickle
import io

def main():

    freq_dict = {}
    with io.open("freq.lem", 'r', encoding="utf8") as f:
        file = f.readlines()
    for line in file:
        splitted_line = line.split()
        freq_dict[splitted_line[0]] = splitted_line[1]

    with open("Wablieft_freq.p", "wb") as file:
        pickle.dump(freq_dict, file)



if __name__ == '__main__':
    main()