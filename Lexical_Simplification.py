import pickle
from cornetto.cornet import Cornet
import operator

def simple_corpus_frequency(word):
    """Returns the frequency of a given word in a simple corpus. If the word doesn't exist, it returns zero."""
    with open("Wablieft_freq.p", "rb") as file:
        freq_dict = pickle.load(file)
    if word in freq_dict.keys():
        return freq_dict[word].encode("utf-8")
    else:
        return "0"
    
def difficult_corpus_frequency(word):
    """Returns the frequency of a given word in a difficult corpus. If the word doesn't exist, it returns zero."""
    with open("Celex_freq.p", "rb") as file:
        freq_dict = pickle.load(file)
    if word in freq_dict.keys():
        return freq_dict[word]
    else:
        return "0"

def word_length(word):
    """Takes a word as input and returns the length of that word."""
    return len(word)

def get_lemma(word):
    """Returns the lemma of a given word based on a pickle file."""
    with open("Lemmas_text.p", "rb") as file:
        lemma_dict = pickle.load(file)
    if word in lemma_dict:
        return lemma_dict[word]
    else:
        return word

def find_synonyms(complex_word):
    """Takes a word as input and returns all the synonyms for that word."""
    c = Cornet()
    c.open(r"cdb2.0.lu.stripped.xml", r"cdb2.0.syn.stripped.xml")
    synoniemen = c.ask(complex_word + " synonym")
    if not synoniemen:
        return {}
    else:
        syn_list = []
        keys = synoniemen.keys()
        for key in keys:
            if 'SYNONYM' in synoniemen[key].keys():
                for keys in synoniemen[key]['SYNONYM'].keys():
                    key_list = keys.split(':')
                    syn_list.append(key_list[0])
        return syn_list

def simpelizer(complex_word, list_synonyms):
    """Removes the synonyms for a given list that are more difficult than the complex word"""
    cw_lemma = get_lemma(complex_word)
    simp_freq_cw = int(simple_corpus_frequency(cw_lemma))
    diff_freq_cw = int(difficult_corpus_frequency(cw_lemma))
    simple_synonyms = []
    for word in list_synonyms:
        s_freq = int(simple_corpus_frequency(word))
        d_freq = int(difficult_corpus_frequency(word))
        if s_freq >= simp_freq_cw or d_freq >= diff_freq_cw:
            simple_synonyms.append(word)
    return simple_synonyms


def best_synonym(complex_word, list_synonyms):
    """ Select the best synonym to replace the complex word from a given synonyms list. The best synonym is the synonym
        with the highest sum of frequencies."""
    syn_dic = {}
    for word in list_synonyms:
        if word_length(word) >= 2:
            freq1 = int(simple_corpus_frequency(word))
            freq2 = int(difficult_corpus_frequency(word))
            total_freq = int(freq1 + freq2)
            syn_dic[word] = total_freq
    if not syn_dic:
        return complex_word
    else:
        best_word = max(syn_dic.iteritems(), key=operator.itemgetter(1))[0]
        return  best_word

def try_simplify_word(index, sentence):
    """Checks if a word is complex enough to replace and if yes, find a replacement"""
    tw = sentence[index]
    target_word = tw.replace("\xe2\x80\x99s", "").replace("\xe2\x80\x99n", "")
    tw_lemma = get_lemma(target_word)
    tw_simp_freq = int(simple_corpus_frequency(tw_lemma))
    tw_diff_freq = int(difficult_corpus_frequency(tw_lemma))
    sum_freq = tw_simp_freq + tw_diff_freq
    if sum_freq > 450:
        return target_word
    elif tw_diff_freq > 300 and tw_simp_freq > 40: #threshold of frequencies of target_word
        return target_word
    elif target_word.istitle(): #checks if word starts with capital letter
        return target_word
    elif word_length(target_word) < 5:
        return target_word
    else:
        synonyms = find_synonyms(tw_lemma)
        if not synonyms:
            return [target_word, tw]
        else:
            simpler_synonyms = simpelizer(target_word, synonyms)
            if not simpler_synonyms:
                return [target_word, tw]
            else:
                return [best_synonym(target_word, simpler_synonyms), tw]

def main():
    outfile = open("simple_sentences.txt", "w+")

    with open("test_zinnen.txt", 'r') as f:
        file = f.readlines()
    for line in file:
        new_line = ""
        for char in line:
            if char not in ['.', ',', '(', ')', '!', '?', ':', ';']:
                new_line += char
        sentence_list = new_line.rstrip().split(' ')
        print(sentence_list)
        simplified_list = []
        complex_words = []
        for i in range(len(sentence_list)):
            try_change = try_simplify_word(i, sentence_list)
            if isinstance(try_change, basestring):
                simplified_word = try_change
            elif isinstance(try_change, list):
                simplified_word = try_change[0]
                complex_enough = try_change[1]
                complex_words.append(complex_enough)
            simplified_list.append(simplified_word)
        simplified_list.append('\n')
        sentence_list.append('\n')

        outfile.write(' '.join(sentence_list))
        outfile.write(' '.join(simplified_list))
        outfile.write(' '.join(complex_words))
        outfile.write(' ')


if __name__ == '__main__':
    main()
